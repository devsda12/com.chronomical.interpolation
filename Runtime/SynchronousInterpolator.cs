using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.Operations
{
    /// <summary>
    /// Synchronously interpolates values using coroutines
    /// </summary>
    public static class SynchronousInterpolator
    {
        #region Class structs
        public struct ValueCallback<T>
        {
            public T value;
            public float progression;
        }
        #endregion

        #region Callable
        /// <summary>
        /// Interpolates an Int between the start and end value
        /// </summary>
        /// <param name="sender">The Monobehaviour the coroutine will run under</param>
        /// <param name="startValue">The start value</param>
        /// <param name="endValue">The end value</param>
        /// <param name="duration">The duration between the interpolation in seconds</param>
        /// <param name="valueCallback">Calls each frame with the updated value</param>
        /// <param name="routineToStop">Possible running routine to stop</param>
        /// <param name="startingProgression">The starting progression ranging from 0 to 1 (for when not the full duration is required)</param>
        /// <param name="onCompleteCallback">Called on completion</param>
        /// <returns>The new coroutine started</returns>
        public static Coroutine InterpolateInt(MonoBehaviour sender, int startValue, int endValue, float duration, Action<ValueCallback<int>> valueCallback, Coroutine routineToStop = null, float startingProgression = 0, Action onCompleteCallback = null)
        {
            startingProgression = Mathf.Clamp(startingProgression, 0, 1);
            if (routineToStop != null) sender.StopCoroutine(routineToStop);
            return sender.StartCoroutine(InterpolateInt(startValue, endValue, duration, valueCallback, startingProgression, onCompleteCallback));
        }

        /// <summary>
        /// Interpolates a Float between the start and end value
        /// </summary>
        /// <param name="sender">The Monobehaviour the coroutine will run under</param>
        /// <param name="startValue">The start value</param>
        /// <param name="endValue">The end value</param>
        /// <param name="duration">The duration between the interpolation in seconds</param>
        /// <param name="valueCallback">Calls each frame with the updated value</param>
        /// <param name="routineToStop">Possible running routine to stop</param>
        /// <param name="startingProgression">The starting progression ranging from 0 to 1 (for when not the full duration is required)</param>
        /// <param name="edgeSmoothing">Whether to apply edge smoothing to the values</param>
        /// <param name="onCompleteCallback">Called on completion</param>
        /// <returns>The new coroutine started</returns>
        public static Coroutine InterpolateFloat(MonoBehaviour sender, float startValue, float endValue, float duration, Action<ValueCallback<float>> valueCallback, Coroutine routineToStop = null, float startingProgression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            startingProgression = Mathf.Clamp(startingProgression, 0, 1);
            if (routineToStop != null) sender.StopCoroutine(routineToStop);
            return sender.StartCoroutine(InterpolateFloat(startValue, endValue, duration, valueCallback, startingProgression, edgeSmoothing, onCompleteCallback));
        }

        /// <summary>
        /// Interpolates a Vector between the start and end value
        /// </summary>
        /// <param name="sender">The Monobehaviour the coroutine will run under</param>
        /// <param name="startValue">The start value</param>
        /// <param name="endValue">The end value</param>
        /// <param name="duration">The duration between the interpolation in seconds</param>
        /// <param name="valueCallback">Calls each frame with the updated value</param>
        /// <param name="routineToStop">Possible running routine to stop</param>
        /// <param name="startingProgression">The starting progression ranging from 0 to 1 (for when not the full duration is required)</param>
        /// <param name="edgeSmoothing">Whether to apply edge smoothing to the values</param>
        /// <param name="onCompleteCallback">Called on completion</param>
        /// <returns>The new coroutine started</returns>
        public static Coroutine InterpolateVector(MonoBehaviour sender, Vector3 startValue, Vector3 endValue, float duration, Action<ValueCallback<Vector3>> valueCallback, Coroutine routineToStop = null, float startingProgression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            startingProgression = Mathf.Clamp(startingProgression, 0, 1);
            if (routineToStop != null) sender.StopCoroutine(routineToStop);
            return sender.StartCoroutine(InterpolateVector(startValue, endValue, duration, valueCallback, startingProgression, edgeSmoothing, onCompleteCallback));
        }

        /// <summary>
        /// Interpolates a Quaternion between the start and end value
        /// </summary>
        /// <param name="sender">The Monobehaviour the coroutine will run under</param>
        /// <param name="startValue">The start value</param>
        /// <param name="endValue">The end value</param>
        /// <param name="duration">The duration between the interpolation in seconds</param>
        /// <param name="valueCallback">Calls each frame with the updated value</param>
        /// <param name="routineToStop">Possible running routine to stop</param>
        /// <param name="startingProgression">The starting progression ranging from 0 to 1 (for when not the full duration is required)</param>
        /// <param name="edgeSmoothing">Whether to apply edge smoothing to the values</param>
        /// <param name="onCompleteCallback">Called on completion</param>
        /// <returns>The new coroutine started</returns>
        public static Coroutine InterpolateQuaternion(MonoBehaviour sender, Quaternion startValue, Quaternion endValue, float duration, Action<ValueCallback<Quaternion>> valueCallback, Coroutine routineToStop = null, float startingProgression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            startingProgression = Mathf.Clamp(startingProgression, 0, 1);
            if (routineToStop != null) sender.StopCoroutine(routineToStop);
            return sender.StartCoroutine(InterpolateQuaternion(startValue, endValue, duration, valueCallback, startingProgression, edgeSmoothing, onCompleteCallback));
        }

        /// <summary>
        /// Interpolates a Vector and a Quaternion between the start and end value
        /// </summary>
        /// <param name="sender">The Monobehaviour the coroutine will run under</param>
        /// <param name="startVec">The start vector</param>
        /// <param name="endVec">The end vector</param>
        /// <param name="startQua">The end quaternion</param>
        /// <param name="endQua">The end quaternion</param>
        /// <param name="duration">The duration between the interpolation in seconds</param>
        /// <param name="valueCallback">Calls each frame with the updated value</param>
        /// <param name="routineToStop">Possible running routine to stop</param>
        /// <param name="startingProgression">The starting progression ranging from 0 to 1 (for when not the full duration is required)</param>
        /// <param name="edgeSmoothing">Whether to apply edge smoothing to the values</param>
        /// <param name="onCompleteCallback">Called on completion</param>
        /// <returns>The new coroutine started</returns>
        public static Coroutine InterpolateVecQua(MonoBehaviour sender, Vector3 startVec, Vector3 endVec, Quaternion startQua, Quaternion endQua, float duration, Action<ValueCallback<(Vector3, Quaternion)>> valueCallback, Coroutine routineToStop = null, float startingProgression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            startingProgression = Mathf.Clamp(startingProgression, 0, 1);
            if (routineToStop != null) sender.StopCoroutine(routineToStop);
            return sender.StartCoroutine(InterpolateVecQua(startVec, endVec, startQua, endQua, duration, valueCallback, startingProgression, edgeSmoothing, onCompleteCallback));
        }
        #endregion

        #region Interpolate coroutines
        /// <summary>
        /// Interpolates between two numeric values without smoothing, not constrained to one though
        /// </summary>
        static IEnumerator InterpolateInt(int startValue, int endValue, float duration, Action<ValueCallback<int>> valueCallback, float progression = 0, Action onCompleteCallback = null)
        {
            float startTime = Time.time - progression * duration;
            int currentVal = startValue;
            bool addition = currentVal < endValue;
            do
            {
                progression = (Time.time - startTime) / duration;
                currentVal = (int)(startValue + (endValue - startValue) * progression);
                if (addition ? currentVal > endValue : currentVal < endValue)
                    currentVal = endValue;
                valueCallback.Invoke(new ValueCallback<int>() { value = currentVal, progression = progression });
                yield return null;
            }
            while (addition ? currentVal < endValue : currentVal > endValue);
            onCompleteCallback?.Invoke();
        }

        /// <summary>
        /// Interpolates between two float values with possible smoothing
        /// </summary>
        static IEnumerator InterpolateFloat(float startValue, float endValue, float duration, Action<ValueCallback<float>> valueCallback, float progression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            float startTime = Time.time - progression * duration;
            float currentVal = startValue;
            bool addition = currentVal < endValue;
            do
            {
                progression = (Time.time - startTime) / duration;
                currentVal = edgeSmoothing ? Mathf.SmoothStep(startValue, endValue, progression) : Mathf.Lerp(startValue, endValue, progression);
                if (addition ? currentVal > endValue : currentVal < endValue)
                    currentVal = endValue;
                valueCallback.Invoke(new ValueCallback<float>() { value = currentVal, progression = progression });
                yield return null;
            }
            while (addition ? currentVal < endValue : currentVal > endValue);
            onCompleteCallback?.Invoke();
        }

        /// <summary>
        /// Interpolates between two Vector3 points with possible smoothing
        /// </summary>
        static IEnumerator InterpolateVector(Vector3 startValue, Vector3 endValue, float duration, Action<ValueCallback<Vector3>> valueCallback, float progression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            float startTime = Time.time - progression * duration;
            Vector3 currentVal = startValue;
            do
            {
                progression = (Time.time - startTime) / duration;
                currentVal = edgeSmoothing ? Vector3.Lerp(startValue, endValue, Mathf.SmoothStep(0, 1, progression)) : Vector3.Lerp(startValue, endValue, progression);
                valueCallback.Invoke(new ValueCallback<Vector3>() { value = currentVal, progression = progression });
                yield return null;
            }
            while (!currentVal.IsApproximately(endValue));
            onCompleteCallback?.Invoke();
        }

        /// <summary>
        /// Interpolates between two Quaternion points with possible smoothing
        /// </summary>
        static IEnumerator InterpolateQuaternion(Quaternion startValue, Quaternion endValue, float duration, Action<ValueCallback<Quaternion>> valueCallback, float progression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            float startTime = Time.time - progression * duration;
            Quaternion currentVal = startValue;
            do
            {
                progression = (Time.time - startTime) / duration;
                currentVal = edgeSmoothing ? Quaternion.Lerp(startValue, endValue, Mathf.SmoothStep(0, 1, progression)) : Quaternion.Lerp(startValue, endValue, progression);
                valueCallback.Invoke(new ValueCallback<Quaternion>() { value = currentVal, progression = progression });
                yield return null;
            }
            while (!currentVal.IsApproximately(endValue));
            onCompleteCallback?.Invoke();
        }

        /// <summary>
        /// Interpolates between two quaternion and vector points with possible smoothing
        /// </summary>
        static IEnumerator InterpolateVecQua(Vector3 startVec, Vector3 endVec, Quaternion startQua, Quaternion endQua, float duration, Action<ValueCallback<(Vector3, Quaternion)>> valueCallback, float progression = 0, bool edgeSmoothing = false, Action onCompleteCallback = null)
        {
            float startTime = Time.time - progression * duration;
            Vector3 currentVec = startVec;
            Quaternion currentQua = startQua;
            do
            {
                progression = (Time.time - startTime) / duration;
                currentVec = edgeSmoothing ? Vector3.Lerp(startVec, endVec, Mathf.SmoothStep(0, 1, progression)) : Vector3.Lerp(startVec, endVec, progression);
                currentQua = edgeSmoothing ? Quaternion.Lerp(startQua, endQua, Mathf.SmoothStep(0, 1, progression)) : Quaternion.Lerp(startQua, endQua, progression);
                valueCallback.Invoke(new ValueCallback<(Vector3, Quaternion)>() { value = (currentVec, currentQua), progression = progression });
                yield return null;
            }
            while (!currentVec.IsApproximately(endVec));
            onCompleteCallback?.Invoke();
        }
        #endregion
    }
}