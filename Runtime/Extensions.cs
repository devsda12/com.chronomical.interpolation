using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.Operations
{
    public static class Extensions
    {
        #region Vector3
        public static bool IsApproximately(this Vector3 input, Vector3 other)
        {
            return Mathf.Approximately(input.x, other.x) &&
                   Mathf.Approximately(input.y, other.y) &&
                   Mathf.Approximately(input.z, other.z);
        }
        #endregion

        #region Quaternion
        public static bool IsApproximately(this Quaternion input, Quaternion other)
        {
            return Mathf.Approximately(input.x, other.x) &&
                   Mathf.Approximately(input.y, other.y) &&
                   Mathf.Approximately(input.z, other.z) &&
                   Mathf.Approximately(input.w, other.w);
        }
        #endregion
    }
}